const coursesData = [
	{
		id : "wdc001",
		name : "PHP- Laravel",
		description : "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Nihil dolorum soluta atque saepe accusamus sunt ratione, ipsa? Magni odit corporis quos vero in veritatis, voluptates eligendi atque dicta assumenda laudantium!",
		price : 45000,
		onOffer : true 

	},
	{
		id : "wdc002",
		name : "Python- Django",
		description : "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Nihil dolorum soluta atque saepe accusamus sunt ratione, ipsa? Magni odit corporis quos vero in veritatis, voluptates eligendi atque dicta assumenda laudantium!",
		price : 50000,
		onOffer : true 
	},
	{
		id : "wdc003",
		name : "Java- Springboot",
		description : "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Nihil dolorum soluta atque saepe accusamus sunt ratione, ipsa? Magni odit corporis quos vero in veritatis, voluptates eligendi atque dicta assumenda laudantium!",
		price : 55000,
		onOffer : true 
	},

]

export default coursesData;